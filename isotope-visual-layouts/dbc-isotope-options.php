<?php
// Damien's Admin Panel v2

// with many thanks https://github.com/bueltge/WordPress-Admin-Style

/**
 * You shouldn't be here.
 */
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="wrap">
	<div id="icon-options-general" class="icon32"></div>
	<h2>Isotope Visual Layouts</h2>
	<?php settings_errors();?>
	<div class="metabox-holder has-right-sidebar">
		
<div class="inner-sidebar">

			<div class="postbox">
				<h3><span>Thanks from Damien</span></h3>
				<div class="inside">
		<p>Thanks for installing this. 
		<br /><a target="_blank" href="http://damien.co/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">Damien</a></p> 
		<p>Please add yourself to <a target="_blank" href="http://wordpress.damien.co/wordpress-mailing-list/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">my mailing list</a> to be the first to hear about updates for this plugin.</p>
		<p>Let me and your friends know you installed this:</p>
	<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://wordpress.damien.co/isotope" data-counturl="http://wordpress.damien.co/isotope" data-count="horizontal" data-via="damiensaunders">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>	

				</div>
			</div>

			<div class="postbox">
				<h3><span>Help & Support</span></h3>
				<div class="inside">
					<?php echo 'Thank you for using version ' . ISOTOPEVERSION; ?>	
					<ul>
					<li><a target="_blank" href="http://wordpress.damien.co/isotope/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">Help & FAQ's</a></li>
					<li><a target="_blank" href="http://wordpress.damien.co/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">More WordPress Tips & Ideas</a></li>
					</ul>
				</div>
			</div>
		
			
			
			
			<div class="postbox">
					<h3><span>Isotope Visual Layouts - You Went Pro</span></h3>
					<div class="inside">
					<P> <a href="https://whitetshirtdigital.com/my-account/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">Check your account and re-download your files</a></P>
					</div> <!-- .inside -->
			</div>		
					
			<!-- ... more boxes ... -->
			



		</div> <!-- .inner-sidebar -->

		<div id="post-body">
			<div id="post-body-content">
				<div class="postbox">
					<?php isotope_vpl_plugin_options_page(); ?>
				</div>
		</div> <!-- .inside -->	
					<div class="postbox">
				<h3><span>Plugin Suggestions</span></h3>
				<div class="inside">
				<p>Here's a couple of plugins of mine that I think you'll enjoy.</p>
				<ul>
					<li><a target="_blank" href="http://wordpress.damien.co/dbc-backup-2/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">DBC Backup 2</a> - secure and easy backup for your WordPress SQL database. Automated schedule and delete older backups</li>
					<li><a target="_blank" href="http://whitetshirtdigital.com/shop/visual-layouts-filtrify/?utm_source=WordPress&utm_medium=isotope-pro-installed&utm_content=admin&utm_campaign=Isotope-Layouts">Visual Layouts Filtrify</a> - the best way to add tag and category based filtering to your website.</li>
				</ul>
				</div>
			</div>	
				</div>		
			</div> <!-- #post-body-content -->
		</div> <!-- #post-body -->
	</div> <!-- .metabox-holder -->
</div> <!-- .wrap -->
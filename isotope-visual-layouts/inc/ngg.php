function ch7bt_shortcode_list() {
global $wpdb;
// Prepare query to retrieve bugs from database
$bug_query = 'select * from ' . $wpdb->get_blog_prefix();
$bug_query .= 'wp_ngg_pictures';
$bug_query .= 'ORDER by bug_id DESC';
$bug_items = $wpdb->get_results( $wpdb->prepare( $bug_query ), ARRAY_A );
// Prepare output to be returned to replace shortcode
$output = '';
$output .= '<table>';
// Check if any bugs were found
if ( !empty( $bug_items ) ) {
$output .= '<tr><th style="width: 80px">ID</th>';
$output .= '<th style="width: 300px">Title / Desc</th>';
$output .= '<th>Version</th></tr>';
// Create row in table for each bug
foreach ( $bug_items as $bug_item ) {
$output .= '<tr style="background: #FFF">';
$output .= '<td>' . $bug_item['bug_id'] . '</td>';
$output .= '<td>' . $bug_item['bug_title'] . '</td>';
$output .= '<td>' . $bug_item['bug_version'];
$output .= '</td></tr>';
$output .= '<tr><td></td><td colspan="2">';
$output .= $bug_item['bug_description'];
$output .= '</td></tr>';
}
} else {
// Message displayed if no bugs are found
$output .= '<tr style="background: #FFF">';
$output .= '<td colspan=3>No Bugs to Display</td>';
}
$output .= '</table><br />';
// Return data prepared to replace shortcode on page/post
return $output;
}
=== Isotope Visual Layouts Pro ===

Contributors: damiensaunders
Donate link: 
Tags: List Posts, Category List, List Custom Post, Isotope, Masonry, CSS, visual effect, animation, jquery, filter, HTML5
Requires at least: 3.3
Tested up to: 3.5
Stable tag: 1.8

Add stunning visual effects to your list of posts & custom post types using Isotope. Needs a responsive theme.

== Description ==

Imagine if you can easily change your website without having to change your theme.


**Bring your website to life** - with stunning animation and visual effects thanks to Isotope by Metafizzy. 



You'll have a grid based responsive visual layout and can choose to list all your posts or selected posts.  You can even use custom post types.

Just create a page or post and add the shortcode [dbc_isotope]


**No coding and no theme to modify**
This is not a theme and you don't need to know anything about HTML or CSS or coding. Just activate the plugin and create a new page (or post) this will save you hours of research on the Internet.




### Checkout my other work
* [Damien](http://damien.co/blog?utm_source=WordPress&utm_medium=isotope_pro&utm_content=readme&utm_campaign=WordPress-Plugin) - strategy, technical development and digital marketing
* [Ideas for WordPress](http://wordpress.damien.co/?utm_source=WordPress&utm_medium=isotope_pro&utm_content=readme&utm_campaign=WordPress-Plugin)
* [Plugins for WordPress](http://wordpress.damien.co/plugins?utm_source=WordPress&utm_medium=isotope_pro&utm_content=readme&utm_campaign=WordPress-Plugin)
* [Visual Layouts Filtrify](http://whitetshirtdigital.com/shop/visual-layouts-filtrify/?utm_source=WordPress&utm_medium=isotope_pro&utm_content=readme&utm_campaign=WordPress-Plugin) - adds tag and category based filtering to your Visual Layouts.


== Installation ==


1. Upload the folder `isotope-visual-layouts` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to the Isotope link under 'Appearances' for basic FAQ
4. Create a page or post and add the shortcode [dbc_isotope]
5. Publish the page and view the results


== Frequently Asked Questions ==

= What Shortcodes are Supported? =  
* [dbc_isotope] will default to your most recent 10 posts  
* [dbc_isotope posts=-1] will list all posts  
* [dbc_isotope posts=5 cat=1,3] will list 5 posts in category 1 and 5  
* [dbc_isotope post_type=feedback] will list 10 posts in custom post type called feedback 
* [dbc_isotope order=DESC] defaults to most recent posts first but you can change this to ASC to go with oldest.
* [dbc_isotope paging=on] adds pagination
* [dbc_isotope filtrify=on] If you have Visual Layouts Filtrify you can turn on or turn off the filtering on each page.
* [dbc_isotope posts=20 paging=on extract=on] - this will include a part of the excerpt. Warning - don't set your posts per page too high or you'll run out of memory.




= Do I need a Commercial licence to use this? =

If you brought a Pro / Developer plugin from me .. then you do need a commercial licence from Metafizzy. 

Please remember that I paid for a licence so you can use this for free (if it's for your personal site).


= Does this work with Post Formats? =
Unfortunately many theme developers don't support them, so I've not supported Post Formats either.


= When I resize my browser window smaller nothing happens =

Hmm it sounds like your theme has fixed widths. You need a responsive theme for the resize animation effect to work.

== Screenshots ==

1. Isotope Settings Page -- not much too it really just some helpful tips to get you started
2. The finished page showing 10 recent posts. Resize your browser window to see them animate.

== Changelog ==

= 1.8 =
* MAJOR - Google Event tracking on the anchor link has been added (disabled by default)
* MAJOR - Twitter Bootstrap replaces my own CSS :)
* switched to using span as a class and not using thumb, medium or large classes anymore

= 1.7 =
* MAJOR - Infinite Scroll disabled for now
* MAJOR - Get_the_excerpt is disabled for now (causes PHP Out of Memory issues)
* MAJOR - uses a minified version of Isotope with my own jQuery added
* Added Pagination Shortcode Attribute  use paging=on  or paging=off
* Added Filtrify Shortcode Attribute use filtrify=on or filtrify=off
* Added Click to resize (jquery + css)
* Added HTML5 data-datepub attribute
* Added Featured Image Size metabox to Add New Post - now you can choose the size of the image on your visual layout.
* Added user option to disable check for new plugin updates (enabled by default). Note this is not a tracker, it just checks to see if there is a new plugin version.

= 1.3 =
* MAJOR - Infinite Scroll enabled & tested
* Big Change - using different class for selectors
* Big Change - stopped using DIV switched to UL
* Big Change - shortcode re-written to WP_Query
* MAJOR - added the Excerpt
* MAJOR - Added Metabox to New Posts (control size of feature image)
* Added Pagination
* Created a number of hooks you can use :)

= 1.0 =
* Introduced 7 colour styles
* 3 options - Image only, Image & Text, Text only
* Re-written the wp_query engine.
* Fixed - can now be used anywhere on a page or post

= 0.33 =
* Added custom update notifications as we're not in WordPress.org anymore.

= 0.32 =
* Fixed - broken Settings link and wrong url in this readme.

= 0.31 =
* Fixed - broken Settings link and wrong url in this readme.

= 0.3 =
* Fixed order - now defaults to most recent posts first.
* Added order shortcode attribute  [dbc_isotope order=ASC]  or order=DESC

= 0.2 =
* Introduces the shortcode [dbc_isotope]
* No need to code or move files
* List posts by category
* List custom post type
* New Isotope licence terms, mean you don't need a licence to use this plugin as-is

= 0.1 =
* Plugin adds the custom stylesheet and Isotope javascript
* Copy / move the isotope_template.php to your theme directory



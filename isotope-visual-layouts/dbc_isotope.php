<?php
/*
Plugin Name: Isotope Visual Layouts Pro
Plugin URI: http://wordpress.damien.co/isotope?utm_source=WordPress&utm_medium=isotope&utm_campaign=Isotope-Layouts
Description: Add visual effects to your list of posts & custom post types using Isotope. Needs a responsive theme
Version: 1.8
Author: Damien Saunders
Author URI: http://damien.co/?utm_source=WordPress&utm_medium=isotope&utm_campaign=Isotope-Layouts
License: This plugin GPLv3 - All changes to the HTML / CSS or Javascript do require a licence.
*/

/**
 * You shouldn't be here. ..
 */
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Variables
 */

define("ISOTOPEVERSION", "1.8");
$plugin = plugin_basename(__FILE__);
global $isotope_vpl_option;
global $isotope_server_host;


/**
 * PHPUnit_test
 * Only create an instance of the plugin if it doesn't already exists in GLOBALS
 */
if
( ! array_key_exists( 'isotope-visual-post-layouts', $GLOBALS ) )
{
	/**
	 *
	 */
	class Isotope_Visual_Post_Layouts
	{
		function __construct()
		{
		} // end constructor
	} // end class
	// Store a reference to the plugin in GLOBALS so that our unit tests can access it
	$GLOBALS['isotope-visual-post-layouts'] = new Isotope_Visual_Post_Layouts();
} // end if





/**
 * isotope_vpl_get_global_options function.
 * GLOBALS - this is more important than you think.
 * @access public
 * @return void
 */
function isotope_vpl_get_global_options()
{
	$isotope_vpl_option  = get_option('isotope_options');
	return $isotope_vpl_option;
}





/**
 * isotope_dev_options function.
 * sets up the local dev for the plugin checker.
 * @access public
 * @return void
 */


$isotope_vpl_option = isotope_vpl_get_global_options();
$isotope_vpl_update = $isotope_vpl_option["plugin_update_checker"];

if (! getenv('MYAPP_ENVIRONMENT'))
{
	$isotope_env = 'production';
	$isotope_server_host ='http://damien.co/isotope_pro.json';
}else
{
	$isotope_env = (getenv('MYAPP_ENVIRONMENT'));
	$isotope_server_host ='http://damien.co/dev_isotope.json';
}



/**
 * Plugin Updater thanks to Janis -
 * visit http://w-shadow.com/blog/2010/09/02/automatic-updates-for-any-plugin/
 * @isotope_server_host is taken from isotope_dev_options
 */

if ($isotope_vpl_update == 'on')
{
	require 'inc/plugin-update/plugin-update-checker.php';
	$IsotopeUpdateChecker = new PluginUpdateChecker(
		$isotope_server_host,
		__FILE__,
		'damien'
	);
}

/**
 * Enqueue isotope.js
 */
function isotope_scripts_method()
{
	wp_enqueue_script('isotope-ds', plugins_url('/js/jquery.isotope-damien.js', __FILE__), array('jquery'),ISOTOPEVERSION,true);
//	wp_enqueue_script('bootstrap', plugins_url('/js/bootstrap.min.js', __FILE__), array('jquery'),ISOTOPEVERSION);
	/* warning .. here be dragons    
	wp_enqueue_script('infinitescroll', plugins_url('/js/jquery.infinitescroll.min.js', __FILE__), array('jquery'),true, true); 
	*/
}

add_action('wp_enqueue_scripts', 'isotope_scripts_method');




/**
 * Enqueue plugin style-file
 */
function dbc_isotope_add_my_stylesheet()
{
	wp_register_style( 'bootstrap', plugins_url('css/bootstrap.css', __FILE__), ISOTOPEVERSION );
	wp_register_style( 'dbc_isotope-style', plugins_url('css/custom_isotope.css', __FILE__), ISOTOPEVERSION );
	wp_enqueue_style( 'dbc_isotope-style' );
		wp_enqueue_style( 'bootstrap' );
}
add_action( 'wp_enqueue_scripts', 'dbc_isotope_add_my_stylesheet' );

/**
 * Add Hook for Menu under Appearance
 */
function dbc_isotope()
{
	global $isotope_admin;
	$isotope_admin = add_theme_page('Isotope', 'Isotope', 'manage_options', 'isotope-admin', 'isotope_vpl_inc_my_page' );
	include dirname(__FILE__).'/inc/help_tab.php';
	// Adds my_help_tab when my_admin_page loads
	add_action('load-'.$isotope_admin, 'my_admin_add_help_tab');
}
add_action('admin_menu', 'dbc_isotope');


/**
 *
 */
function isotope_vpl_inc_my_page()
{
	include dirname(__FILE__).'/dbc-isotope-options.php';
}



/**
 * isotope_visual_post_types_settings_link function.
 * adds direct link to the Isotope Settings page under the Themes menu
 * @access public
 * @param mixed $links
 * @return void
 */
function isotope_visual_post_types_settings_link($links)
{
	$settings_link = '<a href="themes.php?page=isotope-admin">Settings</a>';
	array_unshift($links, $settings_link);
	return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'isotope_visual_post_types_settings_link' );



/* ------------------------------------------------------------------------ *
 * Shortcode Settings
 * ------------------------------------------------------------------------ */









/**
 * Add shortcode function
 * usage example
 * [dbc_isotope posts=5] will show 5 posts
 * [dbc_isotope posts=-1] will show all posts
 * [dbc_isotope posts=-1 post_type=feedback] will show all posts from custom post type feedback
 * [dbc_isotope cats=5] will show 10 posts from category 5
 * [dbc_isotope post_type=pages] will return an isotope layout of your static pages
 * @param posts default is 10
 * @param cats default is all
 */

function dbc_isotope_shortcode_handler($atts)
{
	extract(shortcode_atts(array(
				'posts' => 20,
				'cats' => '',
				'order' => 'DESC',
				'post_type' => '',
				'menu' =>  false,
				'filtrify' => 'off',
				'extract' => 'off',
				'paging' => '',
			),
			$atts));
	$ds_cats2 = $cats;
	$ds_posttype = $post_type;
	$ds_order = $order;
	$ds_menu = $menu;
	$ds_extract = $extract;
	$ds_paging = $paging;
	$ds_filtrify = $filtrify;
	/**
	 * isotope_vpl_option
	 *
	 * (default value: isotope_vpl_get_global_options())
	 *
	 * @var mixed
	 * @access public
	 *
	 */
	$isotope_vpl_option = isotope_vpl_get_global_options();
	$isotope_vpl_style = '';
	$ds_style = '';
	$ds_style = $isotope_vpl_option["dropdown1"];
	if ($ds_style != 'Custom')
		{$isotope_vpl_style = $ds_style;}
	
	$isotope_vpl_images = $isotope_vpl_option["dropdown2"];

	global $wp_query, $paged, $post, $id, $blogid;
	global $damien_filtrify_placeholder;
	
	//setting up the empty variables that we want later
	$isotope_vpl_return = '';
	$damien_fixed_filter = '';
	$damien_custom_filter = '';
	$thumbv='';
	$cat_class='';
	$tag_classes='';
	
	// @TODO FIX THIS in the next version
	if
	($damien_custom_filter == true)
	{
		require 'inc/fixed-filter.php';
	}



	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = (array(
			'post_type' => $ds_posttype,
			'orderby' => 'date',
			'order' => $ds_order,
			'cat' => $ds_cats2,
			'posts_per_page' => $posts,
			'paged' => $paged,

		));

	$isotope_posts = new wp_query($args);


	$big = 999999999; // need an unlikely integer
	$pagination = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $isotope_posts->max_num_pages
		) );


	if
	($isotope_posts->have_posts())
	
	$isotope_vpl_return .= $damien_fixed_filter;

	$isotope_vpl_return ='<!-- Isotope for WordPress by Damien http://wordpress.damien.co/isotope  -->';
	$isotope_vpl_return .= $damien_filtrify_placeholder;
	$isotope_vpl_return .= $damien_fixed_filter;
	$isotope_vpl_return .= '<ul class="isocontent thumbnails">';
	while
	($isotope_posts->have_posts()) : $isotope_posts->the_post();
	
	
	
	/**
	 * Twitter Bootstrap amazing column widths
	 * 
	 * (default value: 'span3')
	 * 
	 * @var string
	 * @access public
	 */
	$span ='span';
	$ds_meta = get_post_meta( $id, '_size', true );
	
	switch ($ds_meta)
	{
	case 'thumbnail'; 
		$thumbv ='small';
		$span .= 3;
		break;
		
	case 'medium';
		$thumbv ='medium';
		$span .= 5;
		break;
	
	case 'large';
		$thumbv ='large';
		$span .= 7;
		break;
		
	default;
		$span .= 3;
		break;
	}
			
	if
	($ds_meta != '')
	{
		$thumbv = $ds_meta;
	}
	$cus_colour = ' box '.$isotope_vpl_style.' ';
	$cat_class = implode(', ', wp_get_post_categories( get_the_ID(), array('fields' => 'names') ) );
	$tag_classes = implode(', ', wp_get_post_tags( get_the_ID(), array('fields' => 'names') ) );
	
	$data_attrib = '';
	$data_attrib = 'data-pubDate="'.get_the_date('Y-m-d H:i:s').'"';
	
	$feat_excerpt = '';
	
	if
	($ds_extract == 'on')
	{
		$feat_excerpt = '<p>'.get_the_excerpt().'</p>';
	}
	else ($feat_excerpt = '');	
	
	/**
	 * feat_filtrify
	 * 
	 * check if the shortcode attribute filtrify=on -- then loop through the tags & categories
	 * 
	 * @var string
	 * @access public
	 */
	$feat_filtrify ='';
	if
	($ds_filtrify =='on')
	{
		if (empty($tag_classes))
		{
		$feat_filtrify =' data-category="'.$cat_class.'"'; 
		}	
		elseif (empty($cat_class))
		{
		$feat_filtrify =' data-tag="'.$tag_classes. '"'; 
		}
		else 
		{
		$feat_filtrify =' data-tag="'.$tag_classes. '" data-category="'.$cat_class.'"';
		}
	}
	else (wp_dequeue_script('filtrify'));
	
	$vpl_pagination = '';
	if
	($ds_paging == 'on')
	{
		$vpl_pagination = '<div class="pagination-small" role="navigation">'.$pagination.'</div>';
	}
	else ($vpl_pagination = '');
	
	
	
	
	switch ($isotope_vpl_images)
	{
	case 'Image Only'; // try this with a photoblog or custom post type
		$feat_excerpt ='';
		$feat_title ='';
		$feat_image = get_the_post_thumbnail($id, $thumbv);
		break;

	case 'Image with Text'; // the default option
		$feat_title = '<a href="'.get_permalink().'"><h3>'.get_the_title().'</h3></a>';
		$feat_image = get_the_post_thumbnail($id, $thumbv);
		$feat_excerpt;
		break;

	case 'Text Only'; // No Image
		$feat_title = '<h3>'.get_the_title().'</h3>';;
		$feat_image = '';
		$feat_excerpt;
		break;

	}

	$isotope_vpl_return .='<li class="'. $span.' '. implode(' ', get_post_class($cus_colour, $post->ID)).'"';

	$isotope_vpl_return .= $feat_filtrify;
	$isotope_vpl_return .= $data_attrib;
	$isotope_vpl_return .='>';
	$isotope_vpl_return .= '<div class="thumbnail">';	
		$isotope_vpl_return .= '<div class="caption"><span style="text-align:center;">';	
		$isotope_vpl_return .= $feat_image;
		$isotope_vpl_return .= $feat_title;	
		$isotope_vpl_return .= $feat_excerpt;
		$isotope_vpl_return .= '<h5 class="infogrid"><span style="color:#33FFCC; background:#4d4d4d; padding: 5px 5px;">'.get_the_category( $id ).'</span>&nbsp;'.get_the_date(('d·m·Y')).'</h5>'; 					
		//$isotope_vpl_return .= '<a class="btn" href="'.get_permalink().'"'; 
	// uncomment next line if you want Google Event Tracking on the button clicks
	//	$isotope_vpl_return .= ' onClick="_gaq.push([\'_trackEvent\', \'Isotope\', \'Layout\', \''.get_the_title().'\'])"';
	// change the text on the button
		//$isotope_vpl_return .= '>more</a>';
		$isotope_vpl_return .= '</div>';
	$isotope_vpl_return .= '</div>';	
	$isotope_vpl_return .= '</li>';

	endwhile;
	$isotope_vpl_return .='</ul>';
	$isotope_vpl_return .= $vpl_pagination;
	wp_reset_query();
	return $isotope_vpl_return;

	$wp_query = null;
	$wp_query = $temp;


	//var_dump($isotope_posts);






}

add_shortcode('dbc_isotope', 'dbc_isotope_shortcode_handler');






/* ------------------------------------------------------------------------ *
 * Isotope Database Defaults
 * ------------------------------------------------------------------------ */








/**
 * Register Activation Hook called on Plugin Init.
 * Define default option settings
 * @access public
 * @return void
 */
register_activation_hook(__FILE__, 'isotope_vpl_set_default_options');





/**
 * isotope_vpl_set_default_options function.
 * Add the default settings to the database
 * @access public
 * @return void
 * @deprecated ['plugin_update_checker'] currently does nothing but in the future it will
 */
function isotope_vpl_set_default_options()
{
	if (get_option('isotope_options') === false)
	{
		$new_options['dropdown1'] = "Yellow";
		$new_options['dropdown2'] = "Image with Text";
		$new_options['version'] = ISOTOPEVERSION;
		$new_options['plugin_update_checker'] = "on";
		$new_options['plugin_usage_checker'] = "on";
		$new_options['inc_excerpt'] = "on";
		// $new_options['chkbox1'] = "on";
		add_option('isotope_options', $new_options);
		add_option('damien_style', "isotope");
	}else
	{
		$existing_options = get_option( 'isotope_options' );
		if (isset($existing_options['version']) === "1.0")
		{
			$existing_options['plugin_update_checker'] = "on";
			$existing_options['version'] = ISOTOPEVERSION;
			$existing_options['plugin_usage_checker'] = 0;
			$existing_options['inc_excerpt'] = "on";
			$existing_options['chkbox1'] = "on";
			update_option( 'isotope_options', $existing_options );
		}
	}
}
add_action('admin_init', 'isotope_vpl_set_default_options');







/* ------------------------------------------------------------------------ *
 * Isotope Admin uses Settings API
 * ------------------------------------------------------------------------ */




/**
 * isotope_vpl_plugin_admin_init function.
 * Register the settings and such
 * @TODO re-enable the validation function
 * @access public
 * @return void
 */
function isotope_vpl_plugin_admin_init()
{
	register_setting( 'isotope_vpl_plugin_options', 'isotope_options', 'isotope_vpl_validate_options');

	//isotope options
	add_settings_section('isotope_vpl_plugin_main', 'Plugin Settings', 'isotope_vpl_plugin_section_text', 'dbc_isotope');

	add_settings_field('isotope_vpl_drop_down1', 'Colour', 'isotope_vpl_setting_dropdown_fn', 'dbc_isotope', 'isotope_vpl_plugin_main');
	add_settings_field('isotope_vpl_drop_down2', 'Featured Image', 'isotope_vpl_setting_thumbnails_fn', 'dbc_isotope',
		'isotope_vpl_plugin_main');
	//add_settings_field('isotope_vpl_chk3', 'Include Excerpt:', 'excerpt_chk2_fn', 'dbc_isotope', 'isotope_vpl_plugin_main');
	//plugin usage and update notifications
	add_settings_section('isotope_vpl_plugin_filter', 'Excerpts, Filtering & Infinite Scroll', 'isotope_vpl_filter_section_text', 'dbc_isotope');
	add_settings_section('isotope_vpl_plugin_settings', 'Update Notification & Usage Stats', 'isotope_vpl_usage_section_text', 'dbc_isotope');
	add_settings_field('isotope_vpl_chk1', 'Enable Update Notifications', 'update_chk2_fn', 'dbc_isotope', 'isotope_vpl_plugin_settings');
	//add_settings_field('isotope_vpl_chk2', 'Send Plugin Usage Stats', 'usage_chk2_fn', 'dbc_isotope', 'isotope_vpl_plugin_settings');
}

add_action('admin_init', 'isotope_vpl_plugin_admin_init');

/**
 * Main Settings Section description
 */
function isotope_vpl_plugin_section_text()
{
	echo '<div class="inside"><p>Select a colour theme and style.</p></div>';
}


/**
 *
 */
function setting_chk1_fn()
{
	$options = get_option('isotope_options');
	if
	($options['chkbox1'])
		{ $checked = ' checked="checked" '; }
	echo "<input ".$checked." id='plugin_chk1' name='isotope_options[chkbox1]' type='checkbox' />";
}


/**
 * isotope_vpl_setting_dropdown_fn function.
 * CSS Colour Style
 * @access public
 * @return void
 */
function isotope_vpl_setting_dropdown_fn()
{
	$options = get_option('isotope_options');
	$items = array("Red", "Green", "Blue", "Orange", "White", "Violet", "Yellow", "Custom");
	echo "<select id='isotope_vpl_drop_down1' name='isotope_options[dropdown1]'>";
	foreach
	($items as $item)
	{
		$selected = ($options['dropdown1']==$item) ? 'selected="selected"' : '';
		echo "<option value='$item' $selected>$item</option>";
	}
	echo "</select>";
}

/**
 * @deprecated Text field reserved for registration key
 */
function isotope_vpl_plugin_setting_string()
{
	$options = get_option('isotope_options');
	echo "<input id='isotope_vpl_plugin_text_string' name='isotope_options[text_string]' size='40' type='text' value='{$options['text_string']}' />";
}


/**
 * isotope_vpl_setting_thumbnails_fn function.
 * Include featured image
 * @access public
 * @return void
 */
function isotope_vpl_setting_thumbnails_fn()
{
	$options = get_option('isotope_options');
	$items = array("Image with Text", "Image Only", "Text Only", "Media Library Photos");
	echo "<select id='isotope_vpl_drop_down2' name='isotope_options[dropdown2]'>";
	foreach
	($items as $item)
	{
		$selected = ($options['dropdown2']==$item) ? 'selected="selected"' : '';
		echo "<option value='$item' $selected>$item</option>";
	}
	echo "</select>";
}

/**
 * update_chk2_fn function.
 *
 * @access public
 * @return void
 */
function update_chk2_fn()
{	$checked = '';
	$options = get_option('isotope_options');
	if
	($options['plugin_update_checker'])
	{
		$checked = 'checked="checked" '; }
	$html = "<input ".$checked." id='isotope_vpl_chk1' name='isotope_options[plugin_update_checker]' type='checkbox' />";
	$html .= '<label for="isotope_vpl_chk3"> Enable to get notified of new versions</label>';
	echo $html;

}



/**
 * @deprecated usage_chk2_fn function.
 *
 * @access public
 * @return void
 */
function usage_chk2_fn()
{
	$options = get_option('isotope_options');
	if
	($options['plugin_usage_checker'])
	{
		$checked = 'checked="checked" '; }
	$html = "<input ".$checked." id='isotope_vpl_chk2' name='isotope_options[plugin_usage_checker]' type='checkbox' />";
	$html .= '<label for="isotope_vpl_chk3"> Enable to send anonymous stats for just this plugin.</label>';
	echo $html;
}



/**
 * Usage & Update Settings Section description
 */
function isotope_vpl_usage_section_text()
{
	echo '<div class="inside"><p>Get update notifications when there is a new version of the plugin.</p></div>';
}

/**
 *
 */
function isotope_vpl_filter_section_text()
{
	echo '<div class="inside"><p>Get your excerpt, filtering & infinite scroll here.</p></div>';
}


/**
 * isotope_vpl_validate_options function.
 * TODO TIDY THIS UP AND MAKE IT WORK
 * @access public
 * @param mixed $input
 * @return void
 */
function isotope_vpl_validate_options( $input )
{
	  if (!isset($input['plugin_update_checker'])) 
	  $input['plugin_update_checker'] = '';

	return $input;

}







/**
 * isotope_vpl_plugin_options_page function.
 * display the admin options page
 *
 * @access public
 * @return void
 */
function isotope_vpl_plugin_options_page()
{
?>
<form action="options.php" method="post">
<?php settings_fields('isotope_vpl_plugin_options');
	do_settings_sections('dbc_isotope'); ?>

<div class="inside"><input class="button-primary" name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
</form>
	<?php
}





/* ------------------------------------------------------------------------ *
 * Isotope Image Size Metabox
 * ------------------------------------------------------------------------ */




/**
 * add_meta_box function.
 *
 * @access public
 * @return void
 */
function isotope_add_meta_box()
{
	add_meta_box('isotope_meta_box',
		'Isotope Featured Image Size',
		'isotope_vpl_display_image_meta_box',
		'post', 'normal', 'high');
}


/**
 * @param $post
 */
function isotope_vpl_display_image_meta_box($post)
{

	//retrieve the values if saved
	$isotope_meta_box_size = get_post_meta($post->ID, '_size', true);
	echo 'Select the image size for your Isotope Page. You don\'t need to do anything for the default';
?>
	<p>Size:
	<select name="isotope_image_size">
		<option value="thumbnail" <?php selected($isotope_meta_box_size, 'thumbnail');?>>Thumbnail</option>
		<option value="medium" <?php selected($isotope_meta_box_size, 'medium');?>>Medium</option>
		<option value="large" <?php selected($isotope_meta_box_size, 'large');?>>Large</option>
	</select>
	</p>
	<?php
}
add_action('add_meta_boxes', 'isotope_add_meta_box');


/**
 * @param $post_id
 */
function isotope_vpl_save_image_meta_box($post_id)
{
	if (isset($_POST['isotope_image_size']))
	{
		update_post_meta($post_id, '_size', strip_tags($_POST['isotope_image_size']));
	}
}

add_action('save_post', 'isotope_vpl_save_image_meta_box');


/**
 * @param $length
 * @return int
 */
function isotope_vpl_custom_excerpt_length( $length )
{
	return 20;
}
add_filter( 'excerpt_length', 'isotope_vpl_custom_excerpt_length' );

/**
 * @param $more
 * @return string
 */
function isotope_vpl_new_excerpt_more($more)
{
	global $post;
	return ' <a href="'. get_permalink($post->ID) . '" more</a>';
}
//add_filter('excerpt_more', 'isotope_vpl_new_excerpt_more');







/* ------------------------------------------------------------------------ *
 * If you delete the plugin - I'll feel sad
 * ------------------------------------------------------------------------ */



/**
 * Uninstall function
 * removes saved plugin options and the plugin update checker
 */
function isotope_vpl_uninstall()
{
	delete_option('isotope_options');
	delete_option('damien_style');
	delete_option('external_updates-isotope-visual-post-layouts');
}
register_deactivation_hook(__FILE__, 'isotope_vpl_uninstall');
?>
